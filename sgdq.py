import re
import sys
import time

from datetime import datetime, timezone, timedelta
from requests import get

BASE_URL = "https://gamesdonequick.com/schedule"
RE_RUNS = u'<tr>\n<td>(.+)<\/td>\n<td>(.+)<\/td>\n.+\n<td class=".+">(.+)<\/td>'
RE_TZ = u'runTimestamp = .+?\'(.+?)\''
RUN_DATE_FORMAT = "%m/%d/%Y %H:%M:%S"

p = re.compile(RE_RUNS, re.IGNORECASE)
html_source = get(BASE_URL).text
runs = re.findall(RE_RUNS, html_source)
str_tz_offset = re.findall(RE_TZ, html_source)[0]

def get_runs(number=10):
    num_runs = 0
    now_date = datetime.now(timezone(timedelta(hours=-4, minutes=0)))
    # for single_run in runs:
    for i in range(0, len(runs)):
        single_run = runs[i]
        if num_runs >= number:
            break
        the_date = utc_to_local(datetime.strptime(single_run[0], RUN_DATE_FORMAT))
        if the_date > now_date:
            the_run = runs[i-3]
            the_date = utc_to_local(datetime.strptime(the_run[0], RUN_DATE_FORMAT))
            num_runs += 1
            if (num_runs == 3): print("--> ", end="")
            print(str(the_date.strftime("%H:%M %p")) + " - " + "[" + the_run[2] + "] " + the_run[1])

def utc_to_local(utc_dt):
    tz = timezone(timedelta(hours=int(str_tz_offset.split(":")[0]), minutes=int(str_tz_offset.split(":")[1])))
    return utc_dt.replace(tzinfo=tz).astimezone(tz=None)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Syntax: " + sys.argv[0] + " <amount_of_runs>")
        print("Ex. " + sys.argv[0] + " 10")
        sys.exit(1)

    # Get the amount of runs required
    get_runs(int(sys.argv[1]))
